# VenenuX minetest mod `bridges`

**bridges simple** adds simple nodes to buid some siple bridges

## Information

this mod is named `bridges` and adds bridges (only one node wide!), 
slim handrails and a self-building-one-node-wide bridge.

![](screenshot.png?raw=true)

Original forum post https://forum.minetest.net/viewtopic.php?t=3488

## Technical info

This mod does almost the same as Mauvebic's "Nodebox utility" mod 
older called "nbu" at http://minetest.net/forum/viewtopic.php?id=2750
But today is lost and this mod was finished when found it

More or less is the same as in Mauvebics. What you get additionally 
are crafting receipes, a large bridge (spans 3 nodes!), and a bridge 
that builds itself automaticly.

* The large bridge (spans 3 nodes):
* Auto-bridge node (looks like the top of a chest from all sides), 

After place and auto bridge node the rest of the bridge gets built automaticly. 
Leftover parts can be found in the chest-like node

#### Dependences

The list of dependencies are in order or relevance and priority to work:

Required to work:

* default

Optional Recommended

* moreblocks
* moon

## Crafting

Crafting recipes:

| simbol | lmeaning egend                                  |
| ------ | ----------------------------------------------- |
| S      | stands for stick                                |
| W      | stands for wooden slab                          |
| V      | stands for either vines or leaves (both can be used) |
| G      | stands for groundplate of bridge                |
| H      | stands for handrail                             |
| -      | stands for "leave empty"                        |

Groundplate: yields 2 "G" parts which are the basis for further bridges:

```
 V V V
 V W V
 V V V
```

Handrail: yields 4 "H" parts which are needed in further receipes:

```
 - - -
 - G -
 - - -
```

small bridge:

```
 S - S
 H G H
 S - S
```

middle part (fits between small bridges):

```
 - - -
 H G H
 - - -
```

corner of a bridge:

```
 S H S
 - G H
 S - S
```

T junction of a bridge:

```
 S H S
 - G -
 S - S
```

end of a bridge.

```
 S H S
 H G H
 S - S
```

handrail, one side closed:

```
 - - -
 - - -
 S H S
```

handrail, two sides closed:

```
 S - -
 H - -
 S H S
```

handrail, three sides closed (if you want to walk around it):

```
 S H S
 H - -
 S H S
```

a large, 3 nodes long bridge:

```
 - small_bridge -
 - middle_part_of_bridge -
 - small_bridge -
```


the self-building automatic bridge:

```
 large_bridge large_bridge large_bridge
 large_bridge large_bridge large_bridge
 large_bridge large_bridge large_bridge
```


## LICENCE

* Code: GPL


    Copyright (C) 2012/2013 Sokomine

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

